import http from './http'

type ReturnData = {
    celsius: number
    fahrenheit: number
}
async function convert(celsius: number): Promise<number> {
    console.log('Service call Convert')
    // result.value = convert(celsius.value)
    console.log(`/temperature/convert/${celsius}`)
    const res = await http.post('/temperature/convert', {
        celsius: celsius
    })

    // const convertResult: ReturnData = result.data
    const convertResult = res.data as ReturnData
    console.log('Service finish call Convert')
    return convertResult.fahrenheit
}
export default { convert }

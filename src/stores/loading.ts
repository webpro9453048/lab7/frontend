import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('counter', () => {

    const isLoading = ref(false)
    const doLoad = () => {
        isLoading.value = true
    }
    const finish = () => {
        isLoading.value = false
    }
    return { isLoading, doLoad, finish }
})

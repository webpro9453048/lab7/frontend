import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([])
  const loadingStore = useLoadingStore()

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finish()
  }

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  async function saveUser(user: User) {
    loadingStore.doLoad()
    if (user.id < 0) {
      console.log('Post ' + JSON.stringify(user))
      const res = await userService.addUser(user)
    } else {
      console.log('Patch ' + JSON.stringify(user))
      const res = await userService.updateUser(user)
    }
    getUsers()
    loadingStore.finish()
  }

  async function deleteUser(user: User) {
    loadingStore.doLoad()
    const res = await userService.delUser(user)
    getUsers()
    loadingStore.finish()
  }
  return { users, getUser, getUsers, saveUser, deleteUser }
})
